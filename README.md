# Become a Git Black Belt

This is a presentation using reveal.js and can be served with grunt-cli.

See the reveal.js documentation for more details
http://lab.hakim.se/reveal-js/#/

## Demo Details

These are the things that were talked about for the live demo part of the
presentation.

* git init
* git add
* git commit

* git add / commit

---

### Talk about commit messages

* git push

* git fetch
* git push / pull / rebase

---

* Two repos
* Add a change to each
* Push one
* Sync them up using rebase

---

### Making a branch

* Add a WIP commit to a repo
* Put it on a feature branch
* Wipe out the old commit from master
* Fetching the branch and rolling back the WIP

---

### Amending a commit

---

### Using Reset

* Add two commits to a repo
* One is a WIP
* Show fixing the situation with a reset

---

* Interactive rt commebase
* Add two commits to a repo, an earlier one with a bad author
* Fix the earlier one with the x command

---

* Ooops
* Do a reset --hard badly
* Show how to recover with the reflog
